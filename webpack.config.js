module.exports = {
    entry: "./src/ts/index.ts",
    output: {
        filename: "bundle.js",
        path: __dirname + "/dist"
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".js", ".json"]
    },

    module: {
        rules: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            // El orden si importa, empieza a cargar desde el último y va transpilando de loader a loader
            { test: /\.scss?$/, use: [
                'style-loader',
                'css-loader',
                'sass-loader'
            ]}
        ]
    },

};